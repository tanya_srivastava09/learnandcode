using System;
using System.Text.Json;
using System.Net;
using System.IO;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

//<summary> Assignment to get the blog name and print the information</summary>
namespace Assignment3
{
   public class BlogInformation
    {
        //<summary> Prints the details returned in json response</summary>
        public void DetailsOfBlog(dynamic jsonObject)
        {
            try
            {
                Console.WriteLine("title :" + jsonObject["tumblelog"]["title"].ToString());
                Console.WriteLine("name :" + jsonObject["tumblelog"]["name"].ToString());
                Console.WriteLine("description :" + jsonObject["tumblelog"]["description"].ToString());
                Console.WriteLine("number of posts :" + jsonObject["posts-total"].ToString());
                JArray postsLength = (JArray)jsonObject["posts"];
                for (int index = 0; index < postsLength.Count; index++)
                {
                    Console.WriteLine(jsonObject["posts"][index]["photo-url-1280"].ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }
        //<summary>Main class to execute all the functions</summary>
        static void Main(string[] args)
        {
            var client = new WebClient();
            Console.WriteLine("Enter the blog name");
            string tumblrBlogName = Console.ReadLine();
            Console.WriteLine("Enter Min range");
            string minRange = Console.ReadLine();
            Console.WriteLine("Enter the max range");
            string maxRange = Console.ReadLine();
            string tumblrUrl = "https://" + tumblrBlogName + ".tumblr.com/api/read/json?photo&num=" + maxRange + "&start" + minRange;
            string response = client.DownloadString(tumblrUrl);
            response = response.Replace("var tumblr_api_read = ","");    //Removing the unwanted characters to make it a valid json
            response = response.Replace(";", "");
            dynamic jsonObject = JsonConvert.DeserializeObject<dynamic>(response);
            BlogInformation blog = new BlogInformation();
            blog.DetailsOfBlog(jsonObject);
            Console.ReadKey();
        }
    }
}